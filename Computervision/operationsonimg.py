from cv2 import cv2 as cv
img=cv.imread('horse.jpeg')
cv.imshow("krishna",img)
cv.waitKey()
cv.destroyAllWindows()
color=(240,150,240) # Color of the rectangle
cv.rectangle(img, (10,10),(30,30),color,thickness=10, lineType=8)
#(10,10) are (x,y) coordinates for the top left point of the rectangle
#(30, 30) are (x,y) coordinates for the bottom right point
cv.imshow("rectangle",img)
cv.waitKey()
cv.destroyAllWindows()
color=(50,200,100)
font=cv.FONT_HERSHEY_SCRIPT_COMPLEX
cv.putText(img, 'Horse on Fire ',(50,100), font, 1, color,thickness=2, lineType=20)
cv.imshow("putText",img)
cv.waitKey()
cv.destroyAllWindows()

