from cv2 import cv2

image=cv2.imread("horse.jpeg")
cv2.imshow("horse",image)
cv2.waitKey()
image_scaled=cv2.resize(image,None,fx=4,fy=4,interpolation=cv2.INTER_CUBIC)
cv2.imshow("resizedimage",image_scaled)
cv2.waitKey()
cv2.destroyAllWindows()
