from cv2 import cv2
image=cv2.imread("horse.jpeg")
cv2.imshow("horse",image)
cv2.waitKey()
height,width=image.shape[:2]
# from which point image should rotate (width/2,height/2)
rotation_matrix=cv2.getRotationMatrix2D((width/1,height/1),45,.5)
rotated_image=cv2.warpAffine(image,rotation_matrix,(width,height))
cv2.imshow("rotation",rotated_image)
cv2.waitKey()
cv2.destroyAllWindows()
