from cv2 import cv2
image=cv2.imread("horse.jpeg")
cv2.imshow("horse",image)
cv2.waitKey()
# second argument is the threshold value which is used to check with every pixel
# Third argument is the maximum value which is assigned to pixel value if it exceeds the threshold
#_,thresh1=cv2.threshold(image,127, 255, cv2.THRESH_BINARY)
th2 =cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 2)
cv2.imshow("thresholdingimage",th2)
cv2.waitKey()
cv2.destroyAllWindows()
