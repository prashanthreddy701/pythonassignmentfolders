from cv2 import cv2
import numpy as np
image=cv2.imread("horse.jpeg")
cv2.imshow("horse",image)
cv2.waitKey()
height,width=image.shape[:2]
new_height,new_width=height/4,width/4
T=np.float32([[1,0,new_width],[0,1,new_height]])
image_translation=cv2.warpAffine(image,T,(width,height))
cv2.imshow("translation",image_translation)
cv2.waitKey()
cv2.destroyAllWindows()
