#In other words, if the input to the activation function is greater than a threshold,
#then the neuron is activated, else it is deactivated,
# i.e. its output is not considered for the next hidden layers
import numpy as np
def binary_step(binary_number):
    if binary_number<0:
        return 0
    else:
        return 1
print(binary_step(5))
print(binary_step(-1))


# Sigmoid transforms the values between the range of  0 and 1
def sig(sigmoid_number):
    return 1/(1 + np.exp(-sigmoid_number))
result_value=sig(2)
print(result_value)
# It doesnot activate all the neurons at the same time
# for the negative values the neurons doesnot get activated

# result will become zero for negative values
def relu_function(relu_value):
    if relu_value<0:
        return 0
    else:
        return relu_value
print(relu_function(7))

# range here is from -1 to 1
def tanh_function(tanh_value):
    result_tanh = (2/(1 + np.exp(-2*tanh_value))) -1
    return result_tanh
result_tanh_value=tanh_function(0.5)
print(result_tanh_value)

# used for multiclass
# it returns the probability of each class
def softmax_function(softmax_value):
    result_value_softmax = np.exp(softmax_value)
    result_value_softmax = result_value_softmax/result_value_softmax.sum()
    return result_value_softmax
result_softmax=softmax_function([0.8, 1.2, 3.1])
print(result_softmax)
