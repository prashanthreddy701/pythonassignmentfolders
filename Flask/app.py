# import the flask class
import json
import sqlite3
import os
from flask import Flask, redirect , url_for, render_template, jsonify, request


app=Flask(__name__)  # creating an instance of the class
# we need to communicate with database so here I have created a function
def db_connection():
    conn = None
    try:
        conn = sqlite3.connect("books.sqlite")
    except Exception as e:
        print(e)
    return conn


@app.route("/books", methods=["GET", "POST"])
def books():
    conn = db_connection()
    # Creating a cursor object using the cursor() method
    # by this we can execute methods from sqlite
    conn.cursor()

    if request.method == "GET":
        cursor = conn.execute("SELECT * FROM book")
        books = []
        for row in cursor.fetchall():
            books.append(dict(id=row[0], author=row[1], language=row[2], title=row[3]))
        if books is not None:
            return jsonify(books)

    if request.method == "POST":
        new_author = request.form['author']
        new_lang = request.form['language']
        new_title = request.form['title']
        sql = """INSERT INTO book (author, language,title)
            VALUES (?,?,?)
        """
        cursor = conn.execute(sql, (new_author, new_lang, new_title))
        conn.commit()
        return f"Book with the id: {cursor.lastrowid} created successfully"


@app.route("/book/<int:id>", methods=["GET", "PUT", "DELETE"])
def single_book(id):
    conn = db_connection()
    cursor = conn.cursor()
    book = None
    if request.method == "GET":
        cursor.execute("SELECT * FROM book WHERE id=?", (id,))
        rows = cursor.fetchall()
        for r in rows:
            book = r
        if book is not None:
            return jsonify(book), 200
        else:
            return "Something wrong is ", 404

    if request.method == "PUT":
        sql = """UPDATE book SET
                title=?,
                author=?,
                language=?
            WHERE id=?"""
        author = request.form["author"]
        language = request.form["language"]
        title = request.form["title"]
        updated_book = {
            "id": id,
            "author": author,
            "language": language,
            "title": title
        }
        conn.execute(sql, (author, language, title, id))
        conn.commit()
        return jsonify(updated_book)

    if request.method == "DELETE":
        sql = """ DELETE FROM book  WHERE id=?"""
        conn.execute(sql, (id,))
        conn.commit()
        return "The book  with id:{} has been deleted ".format(id, ), 200


@app.route("/")  # default port address
def home():
    return render_template("home.html")

@app.route("/add",methods=["POST","GET"])
def add():
    return render_template("login.html")

@app.route("/admin")
def admin():
    return redirect(url_for("home"))  # it redirects to home
  # This line ensures that our Flask app runs only
  #  when it is executed in the main file
  #  and not when it is imported in some other files
if __name__=="__main__":
    # app.run()  # app.run() makes our application run
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)

