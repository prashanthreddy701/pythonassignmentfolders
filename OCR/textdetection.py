from cv2 import cv2
import pytesseract
# read image
img = cv2.imread('handwriting.png')
img = cv2.resize(img, (400, 450))
cv2.imshow("Image", img)
# configurations
CONFIG = ('-l eng --oem 1 --psm 3')
# pytessercat
text = pytesseract.image_to_string(img, config=CONFIG)
# print text
text = text.split('\n')
print(text)
cv2.waitKey()
cv2.destroyAllWindows()
