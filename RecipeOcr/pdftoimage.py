from pdf2image import convert_from_path
import os
# Store Pdf with convert_from_path function
images = convert_from_path('Recipe-Book.pdf')
try:
	if not os.path.exists('pet'):
		os.makedirs('pet')
except OSError:
	print ('Error')

for i in range(len(images)):

      # Save pages as images in the pdf
    images[i].save('./pet/page'+ str(i) +'.jpg', 'JPEG')