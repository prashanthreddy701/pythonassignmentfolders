import os
from flask import Flask, render_template, request
import cv2
import pytesseract
from flask import Flask
import redis

r = redis.Redis(host='localhost', port=6379, db=1)

app = Flask(__name__)
#run_with_ngrok(app)  # Start ngrok when app is run
def ocr_output(IMG):
    TEXT = pytesseract.image_to_string(IMG)
    return TEXT

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

@app.route("/")
def index():
    return render_template("upload.html")

@app.route("/upload", methods=['POST'])
def upload():
    target = os.path.join(APP_ROOT, 'images/')
    print(target)

    if not os.path.isdir(target):
        os.mkdir(target)

    for file in request.files.getlist("file"):
        print(file)
        filename = file.filename
        destination = "".join([target, filename])
        print(destination)
        file.save(destination)
        img = cv2.imread(destination)
        #cv2.imshow("org",img)
        #print('Original Dimensions : ', img.shape)
        scale_percent = 30 # percent of original size
        width = int(img.shape[1] * scale_percent / 100)
        height = int(img.shape[0] * scale_percent / 100)
        dim = (width, height)
        # resize image
        resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
        #print('Resized Dimensions : ', resized.shape)
        #cv2.imshow("Resized image", resized) #showing Resized image
        croptitle = resized[270:350, 0:740] #giving image sizes (Crop)
        #cv2.imshow('CropImage1', croptitle)
        cropingred = resized[400:800, 0:470]
        #cv2.imshow('CropImage2', cropingred)
        cropsteps = resized[400:800, 470:740]
        #cv2.imshow('CropImage3', cropsteps)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        RES = ocr_output(croptitle)
        fin_res1 = "Title of the Recipe : " + RES
        print(fin_res1)
        setr = [] #creating a empty list
        fin_res = "\n "
        fin_res = fin_res +"\n" + fin_res1
        r.set('Recipe Title', fin_res1) # True
        value = r.get('Recipe Title')
        print(value) # b'world'
        print(value.decode())
        setr.append(fin_res1) #passing value in to list
        #with open('/home/neosoft/Desktop/Machine Learning Practice/Ocr/Recipe-Assignment/Recipe-solution-Pytesseract/Recipe.csv', 'w', encoding='UTF8') as f:
         #   writer = csv.writer(f)
          #  writer.writerow(setr)
        RES = ocr_output(cropingred)
        fin_res2 = "Ingredients For Recipe : \n" + RES
        print(fin_res2)
        fin_res = fin_res + "\n" + fin_res2
        r.set('Recipe Ingredients', fin_res2) # True
        value2 = r.get('Recipe Ingredients')
        print(value2) # b'world'
        print(value2.decode())
        setr.append(fin_res2)
        #with open('/home/neosoft/Desktop/Machine Learning Practice/Ocr/Recipe-Assignment/Recipe-solution-Pytesseract/Recipe.csv', 'a', encoding='UTF8') as f:
         #   writer = csv.writer(f)
          #  writer.writerow(setr)
        RES = ocr_output(cropsteps)
        fin_res3 = "Cooking Steps For Recipe : " + RES
        print(fin_res3)
        fin_res = fin_res + fin_res3
        r.set('Recipe Cooking Steps', fin_res3) # True
        value3 = r.get('Recipe Cooking Steps')
        print(value3) # b'world' Display in Encode Type
        print(value3.decode()) # Outout Displayed Normally
        setr.append(fin_res3)

    return fin_res
    #return render_template("complete.html")
if __name__ == "__main__":
    app.run(debug=True)