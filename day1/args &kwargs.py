def more_args(*args):
  print(args)
  #hello
 # variable length arguments are passed
 #Variable length arguments are packed as tuple.
more_args(1, 2, 3, 4)
more_args()
more_args(1,2,3,4,5,6,7,8,9,10)

def more_args(**kwargs):
    print(kwargs)
 #variable length of keyword arguments to the function.
    #Variable length kwargs are packed as dictionary.


