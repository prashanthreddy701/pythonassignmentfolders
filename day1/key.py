'''if any function contain Yield keyword then it is known as Generator function
To generate a sequence of values
Generator function wont take any input
 Memory utilisation is the biggest addvantage in generator functions
 genarators are used to read data from large datasets
Yield simply produces a sequence of values. We generally use yield when we want to iterate over a sequence,
but the idea is that the yield method doesn’t store the entire sequence in memory but executes only when they are told '''

def function():
    count=0
      
    for i in range(10):             
        count += i
        yield count
  
for i in function():
    print(i)

print("-------")   
def square(numbers):
    result = []
    for n in numbers:
        result.append(n ** 2)
    return result
    
numbers = [1, 2, 3, 4, 5]
squared_numbers = square(numbers)
print(squared_numbers)
print("---------")
def square(numbers):
    for n in numbers:
        yield n ** 2
numbers = [1, 2, 3, 4, 5]
squared_numbers = square(numbers)
print(squared_numbers)
print("---------")
print(next(squared_numbers))
print(next(squared_numbers))
print(next(squared_numbers))
print(next(squared_numbers))


def square(numbers):
    for n in numbers:
        yield n ** 2
numbers = [1, 2, 3, 4, 5]
squared_numbers = square(numbers)
print(squared_numbers,"squarednumbers")
#The for loop actually calls the next() function under the hood.
for n in squared_numbers:
    print(n)
