import pandas as pd 
import numpy as np 
# returns boolean value 
data_df=pd.read_csv("Data.csv") 
print(data_df.isna().sum())
#print(data_df["Salary"].isna()) 
# fillna is used to fill null values 
values={"Age":30,"Salary":30000} 
print(data_df.fillna(value=values).head(10)) 
# any means if there is any null value then that row will be deleted 
print(data_df.dropna(how="any")) 
print(data_df[0:10].duplicated())

