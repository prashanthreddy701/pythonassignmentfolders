import pandas as pd 
import numpy as np 
data_df=pd.read_csv("Data.csv")
# here I am appending another row to data_df data set 
x=data_df.append({"Country":"India","Age":25,"Salary":10000,"Purchased":"Yes"},ignore_index=True) 
# here i am deleting 1 st row and 2nd row 
y=data_df.drop(index=[1, 2])
print(y)
country_grp = data_df.groupby(['Country'])
# here group object is created 
print(country_grp) 
# so in order to see individual 
group=country_grp.groups
print(group) 
# here i GOT FRANCE GROUP DETAILS 
x=country_grp.get_group('France')
print(x)
# Nan values are not taken into consderation 
print('mean of Salary:', data_df['Salary'].mean())
print('median of Salary:', data_df['Salary'].median())
print('maximum of Salary:', data_df['Salary'].max())
print('total number of Salaries  count:', data_df['Salary'].count())
#Can be used to count the number of times each value is repeated. 
print('count of each distinct :\n', data_df['Country'].value_counts()) 
df = pd.DataFrame([[1, 2, 3],
                   [4, 5, 6],
                   [7, 8, 9]],
                  columns=['A', 'B', 'C']) 
print(df)
x=df.agg(['sum', 'min', 'mean']) 
print(x) 

