import pandas as pd
import numpy as np
# here creating people  dataframe
people = {
    "first": ["Kristen", 'Maxine', 'John'], 
    "last": ["Carol", 'Willians', 'Smith'], 
    "email": ["KristenC@gmail.com", 'Maxine.Williams@email.com', 'JohnSmith@email.com']
}
people_df = pd.DataFrame(people)
# adding first and last to full_name column 
people_df["full_name"]=(people_df["first"]+ "  " + people_df["last"])
print(people_df)
data_df=pd.read_csv("Data.csv")
pd.Series(["Amazon","Flipkart","Ajio"])
# here new column with Store is used and values are inserted to it 
data_df["Store"]=pd.Series(["Amazon","Flipkart","Ajio","jabong","myntra","walmart","more","D-mart","souvenir"]) 
print(data_df)
# in another way
people = {
    "full_name": ["Jack Smith", 'Jane Lodge', 'John Doe', 'Kristen Carol'], 
    "email": ["JackSmith@gmail.com", 'JaneLodge@email.com', 'JohnDoe@email.com', 'KristenC@email.com']
}
people_df = pd.DataFrame(people)
people_df["full_name"].str.split("   ")
people_df[['first', 'last']]=people_df['full_name'].str.split(' ', expand=True)
print(people_df)

age_and_hobbies={
"age":[21,22,23,24],
"hobbies":["movies","reading books","cricket","running"] 
} 
age_and_hobbies_df=pd.DataFrame(age_and_hobbies) 
age_and_hobbies_df
# here peoplle_df and age_and_hobbies dataframes are concatenated  
x=pd.concat([people_df, age_and_hobbies_df], axis=1) 
y=pd.concat([people_df, age_and_hobbies_df], axis="columns") 
print(x)
print(y) 
name = np.array(['Alexis', 'Jonathan'])
gender = np.array(['Female', 'Male']) 
name_series = pd.Series(name)
gender_series = pd.Series(gender) 
user_df = pd.concat([name_series, gender_series], axis=1, keys=['name', 'gender'])
print(user_df) 
people_df.drop(columns=["first","last"], inplace=True) 
print(people_df)
x=people_df.append({"full_name":"prashanth reddy","email":"prashanth.ssjna@gmail.com"},ignore_index=True)
print(x)
