#append
#Adds an element to the end of the list.
list_a = []
for x in range(1,5):
    list_a.append(x)
print(list_a)
print("------------------")
#extend  Adds all the elements of a sequence to the end of the list.
list_a = [1, 2, 3]
list_b = [4, 5, 6]
list_a.extend(list_b)
print(list_a)
print("------------------")
# iNSERT Element is inserted to the list at specified index.
list_a = [1, 2, 3]
list_a.insert(1,4)
print(list_a)
print("------------------")
# pop removes last element 
list_a = [1, 2, 3]
list_a.pop()
print(list_a)
print("------------------")
# remove Removes the first matching element from the list.
list_a = [1, 3, 2, 3]
list_a.remove(3)
print(list_a)
print("------------------")
# clear  Removes all the items from the list.
list_a = [1, 2, 3]
list_a.clear()
print(list_a)
print("------------------")
#index  Returns the index at the first occurrence of the specified value.
list_a = [1, 3, 2, 3]
index =list_a.index(3)
print(index)
print("------------------")
#count  Returns the number of elements with the specified value
list_a = [1, 2, 3]
count = list_a.count(2)
print(count)
print("------------------")
# sort Sorts the list.
list_a = [1, 3, 2]
list_a.sort()
print(list_a)
print("------------------")
# sorted Creates a new sorted list
list_a = [1, 3, 2]
sorted(list_a)
print(list_a)
print("------------------")
list_a = ["R", "B", "G", "O", "W"]
list_b = list_a[0:5:3]
print(list_b)
list_a = ["R", "B", "G", "O", "W"]
list_b = list_a[4:0:-2]
print(list_b)
