print(__name__) # direct execution __main__
#python module1.py this is known as direct execution of the model
'''if __name__=="__main__":
    print("module1 executing directly just ike main program ")

else:
    print("module1 executing indirectly because of import statement")
    
'''
def f1():
    print("f1 function")
    
def f2():
    print("f2 function")
    
def f3():
    print("f3 function")
    
def f4():
    print("f4 functions")
    
if __name__=="__main__":
    f1()
    f2()
    f3()
    f4()
    
