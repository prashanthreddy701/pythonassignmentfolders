# write a program to reverse the given string
s="prashanth"
print(s[::-1])
result=reversed(s)
for ch in result:
    print(ch)
print("--------------------------")
output=""
l=len(s)
for i in range(l):
    output=output+s[l-i-1]
print(output)
print("------------------------")
# program to reverse the words"
word="I LOVE my Country"
w=word.split()
print(*w[::-1])
print(" ".join(w[::-1]))

print("------------------------")
# program to find substring 
full_sequence="abcde"
subsequence="ace"
length_subsequence=len(subsequence)
index=0
for char in full_sequence:
    if (char==subsequence[index]):
        index=index+1 
    if (index==length_subsequence):
        break 
if (index==length_subsequence):
    print("Yes") 
else:
    print("No")
print("------------------------")
a="a4b3c1d1"
output=""
for ch in a:
    if ch.isalpha():
        x=ch
    else:
        d=int(ch)
        output=output+x*d
print(output)

