#Syntax error: errors because of invalid syntax
#Exceptions: while executing the program if something goes wrong

'''
print hello bro
syntax error
'''
'''Exceptions are raised when the program is syntactically correct, but the code resulted in an error.
This error does not stop the execution of the program, however, it changes the normal flow of the program.
'''
# the code which may rise  an exception is called risky code and we should take care of that
'''
Try and except statements are used to catch and handle exceptions in Python. Statements that can raise exceptions are
kept inside the try clause and the statements that handle the exception are written inside except clause.
'''
print("A")
try:
    print(10/0)
except ZeroDivisionError: #no problem if we get zerodivision error
    print(10/2)
print("b")
'''
try:
    stmt1
    stmt2
    stmt3
except XXX:
    stmt-4
stmt-5

case-1: If there is no exception
1,2,3,5 and Normal Termination
case-2: If an exception raised at stmt-2 and corresponding except block matched
1,4,5 Normal Termination
case-3: If an exception raised at stmt-2 and corresponding except block not matched
1, Abnormal Termination
case-4: If an exception raised at stmt-4 or at stmt-5 then it is always abnormal
termination.
Conclusions:
1. within the try block if anywhere exception raised then rest of the try block wont be
executed eventhough we handled that exception. Hence we have to take only risky code
inside try block and length of the try block should be as less as possible.
2. In addition to try block,there may be a chance of raising exceptions inside except and
finally blocks also.
3. If any statement which is not part of try block raises an exception then it is always
abnormal termination.
'''
# finally block executes even if there is an exception handled or not if an error raised or not
#Case-1: If there is no exception
try:
    print("try")
except:
    print("except")
finally:
    print("finally")
#Case-2: If there is an exception raised but handled:
try:
    print("try")
    print(10/0)
except ZeroDivisionError:
    print("except")
finally:
    print("finally")
#Case-3: If there is an exception raised but not handled:
try:
    print("try")
    print(10/0)
except NameError:
    print("except")
finally:
    print("finally")
