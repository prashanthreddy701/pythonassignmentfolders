# match
# match object checks if the given input string matches with parent string
import re
match=re.match("aaa","aaabc")
print(match.start(),"......",match.end())

# fullmatch means complete string should be matched
import re
match=re.match("aaaaaaaa","abcdef")
if match!=None:
    print("matched")
else:
    print("Not matched")
# search will find whether given input is there or not 
import re
match=re.search("aaa","cdaaabc")
print(match.start(),"....",match.end())
# find all To find all occurrences of the match.
# This function returns a list object which contains all occurrence
import re
l=re.findall("[0-9]","a7b9c5kz")
print(l)

#finditer
# Looks for the match anywhere in the string
# Returns objects for all matched substrings as a list
# if there is a match, otherwise returns emptylist
import re
itr=re.finditer("[a-z]","a7h943cde948")
for m in itr:
    print(m.start(),".....",m.end(),"....",m.group())

#sub means substitution
import re
s=re.sub("[a-z]","#","a76d3x3jru")
print(s)

#split split according to particular pattern
import re
l=re.split(" ","sachin,sehwag,gangully")
for t in l:
    print(t)
           
