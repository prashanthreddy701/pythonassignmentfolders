from socket import *
def main():
    host=gethostname()
    port=8088
    s=socket()
    s.bind((host,port))
    print("server is running")
    s.listen(4)
    c,add=s.accept()
    print("connection established")
    msg=c.recv(1024)
    print(str(msg))
    c.send(b'hello bro')
main()
'''
 Sockets can be thought of as endpoints in a communication channel that is
 bi-directional and establishes communication between a server and one or more clients
 AF_INET refers to the address-family ipv4. The SOCK_STREAM means connection-oriented TCP protocol.
 A server has a bind() method which binds it to a specific IP and port so that it can listen to incoming requests on that IP and port.
 A server has a listen() method which puts the server into listening mode. This allows the server to listen to incoming connections.
 And last a server has an accept() and close() method.
 The accept method initiates a connection with the client and the close method closes the connection with the client. 

'''
