# For the existing function we can give another name is known as function aliasing
def wish(name):
    print("goodmorning:",name)
greeting=wish
wish("kejriwals")
greeting("kejriwal")
# wish is a reference variable pointing to function object
# greeting is also acessing to function object
# Function Aliasing for the existing function, we can give another name
del wish
greeting("kejriwal")
# only reference variable deleted not object
# Nested Functions means functions inside functions
def outer():
    print("Outer function started")

    def inner():
        print("inner function started")
    print(" I love my country")
outer()
#Nested Function means function inside function
#Nested Function are local to outer function from outside we cannot call directly
def f1():
    return 10
x=f1()
print(x)

# function can return another function
def outer():
    print("Outer function started")

    def inner():
        print("inner function started")
    print(" I love my country")
    return inner
newf=outer()
newf()
''' Difference between the two lines
'''
def f1(func):
    print("f1 func got {} function as argument".format(func))
    func()
def fx():
    print("fx function")
def fy():
    print("fy function")
f1(fx)# here fx is the function we are passing the function name (means whole function) func variable pointing to fx function

