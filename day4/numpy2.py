'''2,3,1,1) (1,3,1,4)

Two arrays are said to be compaitable in a dimension if

they have the same size in that dimension

or one of the arrays has one in that dimension

array will be stretched in row direction and column direction with same elements [1]=== [1][1][1] [1][1][1] [1][1][1]
'''
#np.ravel()
'''
Returns a contiguous flattened array (A 1D array, containing the elements of the input array).
The shape of the original array will not change.
Returns a view of the original array
'''
import numpy as np
array_2d=np.array([[1,2,3],[4,5,6]])
raveled_array = np.ravel(array_2d)
raveled_array[0] = -10
print("raveled array: \n", raveled_array, "\n")
print("array_2d: \n", array_2d)

'''
ndarray.flatten()
Returns a copy of the array flattened to one dimension.
The shape of the original array will not change.
'''
array_2d = np.array([[1,3,5], 
                     [0,2,4]])
flattened_array = array_2d.flatten()
flattened_array[0] = -10

print("flattened array: \n", flattened_array, "\n")
print("array_2d: \n", array_2d)

'''Using np.newaxis
It is used to increase the dimension of an existing array by one.

nD array becomes (n+1)D array.
It can be used to convert a 1D array to either a row vector or a column vector
'''
a = np.array([1, 3, 5])

row_vector = a[np.newaxis, :]
print("row vector alternative:", a[np.newaxis])
print("shape of original array:        ", a.shape)
print("shape of a[np.newaxis, :]", row_vector.shape)
a = np.array([1, 3, 5])

column_vector = a[:, np.newaxis]
column_vector
print("shape of original array:  ", a.shape)
print("shape of a[:, np.newaxis]:", column_vector.shape)

print("shape of a[:, np.newaxis, np.newaxis]:", a[:, np.newaxis, np.newaxis].shape)
print("shape of a[np.newaxis, :, np.newaxis]:", a[np.newaxis, :, np.newaxis].shape)

'''
np.squeeze
np.squeeze(arr, axis)

# It removes single-dimensional entries from the shape of an array.
'''

a = np.array([[[1], [3], [5]]])
print("shape before squeeze",a.shape)
print("shape after squeeze",np.squeeze(a).shape)

a = np.array([1, 3, 5])
print("original:", a)

expanded = np.expand_dims(a, axis=0)
print("expanded:", expanded)

squeezed = np.squeeze(expanded, axis=0)
print("squeezed:", squeezed)


a = np.array([1, 3, 5])
print("original:", a)

expanded = np.expand_dims(a, axis=1)
print("expanded: \n", expanded)

squeezed = np.squeeze(expanded, axis=1)
print("squeezed:", squeezed)

'''
np.moveaxis
np.moveaxis(arr, original_positions, new_positions)

Returns the view of an array with the axes moved from their original positions to the new positions.
Other axes remain in their original order.
'''
array_3d = np.ones((2, 3, 4, 5, 6))

axis_moved_array = np.moveaxis(array_3d, [0, 1], [1, 2])
print(axis_moved_array.shape)

array_3d = np.array([[[5, 5, 5],
                    [0, 0, 0]],
                     
                    [[1, 2, 3],
                    [-1, -2, -3]],
                     
                    [[6, 7, 8],
                    [-6, -7, -8]]])

print("original array: \n", array_3d, "\n")

moved_axes_array = np.moveaxis(array_3d, 0, 2)
print("array after moving axes: \n", moved_axes_array)

'''
np.swapaxes
np.swapaxes(arr, axis1, axis2)

Returns an array with axis1 and axis2 interchanged.
Other axes remain in their original order.
'''
array_3d = np.array([[[5, 5, 5],
                    [0, 0, 0]],
                     
                    [[1, 2, 3],
                    [-1, -2, -3]],
                     
                    [[6, 7, 8],
                    [-6, -7, -8]]])

print("original array: \n", array_3d, "\n")

swapped_axes_array = np.swapaxes(array_3d, 1, 2)
print("array after swapping axes: \n", swapped_axes_array)

'''
Splitting an array
np.split(arr, indices_or_sections, axis=0)
Splits the given array arr into multiple sub-arrays along the given axis based on indices_or_sections and returns a list of sub-arrays.
If indices_or_sections is an integer say N, then arr will be divided into N equal arrays along the given axis.
'''
'''
Split - Horizontal
np.hsplit(arr, indices_or_sections)
Split the given arr into multiple sub-arrays horizontally i.e column-wise and returns a list of sub-arrays.
It is equivalent to split with axis = 1
'''
array = np.array([1, 2, 3, 4, 5])
split_arrays = np.hsplit(array, [3])

array = np.array([[ 1, -1,  2, -2,  3, -3],
                  [ 1, -1,  2, -2,  3, -3],
                  [ 1, -1,  2, -2,  3, -3]])

split_arrays = np.hsplit(array, [2])
print(split_array)


split_arrays = np.hsplit(array, [1, 2])
print(split_arrays)
'''
Split - Vertical
np.vsplit(arr, indices_or_sections)
Splits the given arr into multiple sub-arrays vertically i.e row-wise and returns a list of sub-arrays.
It is equivalent to split with axis = 0.
.vsplit only works on arrays of 2 or more dimensions.
'''
array = np.array([[1,   1,  1],
                  [-1, -1, -1],
                  [2,   2,  2],
                  [-2, -2, -2],
                  [3,   3,  3],
                  [-3, -3, -3]])

split_arrays = np.vsplit(array, [2])
print(split_arrays)



