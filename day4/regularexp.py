import re
pattern=re.compile("ab")  # compiling string into pattern object 
matcher=pattern.finditer("abaababa")  # returns iterator to find pattern in the target string
for match in matcher:
    print(match.start())
print("The number of occurences is:",)
'''
[abc]=a or b or c
[^abc]=any character except a b c 
[a-z]=a to z everything between
[A-Z]=A to Z everything between
[0-9]=0 to 9
[]?=occurs 0 or 1 times
[]+=occurs 1 or more times
[]* occurs 0 or more times
[]{n} occurs n times
[]{n,} occurs n or more times
[]{y,z} occurs at atleast y times but less than z times
\d =[0-9]
\D=[^0-9]
\w =[a-z A-Z-0-9]
\W=[^\w]
'''
'''matcher=re.finditer('ab','abbaababbaabbcssc')
for match in matcher:
    print(match.start())'''
'''
\s  Space character
\S  Any character except space character
\d  Any digit from 0 to 9
\D  Any character except digit
\w  Any word character [a-zA-Z0-9]
\W  Any character except word character (Special Characters)
.  Any character including special characters'''
import re
s='abc'
m=re.match(s,"abcabdefg")
print("Start Index:",m.start(), "and End Index:",m.end())
s="prashanth"
m=re.fullmatch(s,"ababab")
if m!= None:
    print("Full String Matched")
else:
     print("Full String not Matched")
s="aaa"
m=re.fullmatch(s,"abaaabb")
if m!= None:
    print("Available")
else:
     print("Not available")
import re
l=re.findall("[0-9]","a7b59kz")
print(l)
import re
s=re.sub("[a-z]","$","a7b9c5k8z")
print(s)
import re
t=re.subn("[a-z]","#","a7b9c5k8z")
print(t)
print("The Result String:",t[0])
print("The number of replacements:",t[1])

    
