# Concatenation of arrays
'''
np.concatenate((a1, a2, ...), axis=0)

Joins a sequence of arrays along the given axis and returns the concatenated array.
The arrays must have the same shape, except in the dimension corresponding to the axis.
The resulting array has the same dimensions as that of the input arrays.
'''
import numpy as np

a = np.array([7, 1, 11])
b = np.array([3, 0, 17])
print(a.shape,b.shape) 
print(np.concatenate((a, b)))
# axis 0 (2,3) (1,3) vertical direction 
a = np.array([[7, 4, 5], 
              [1, 3, 2]])
b = np.array([[-1, -2, -3]])
print(a.shape,b.shape)
print(np.concatenate((a, b), axis=1))
# axis 1 (2,3) (2,1) horizontal direction 
a = np.array([[7, 4, 5], 
              [1, 3, 2]])

b = np.array([[-1], 
             [-2]])
print(a.shape,b.shape)
print(np.concatenate((a, b), axis=1))

# Stacking - Vertical
'''
np.vstack((a1, a2, ...))
Stacks the arrays a1, a2, … vertically (row-wise) in sequence and returns the stacked array.
Except in the case of 1D arrays, it's equivalent to np.concatenate along axis = 0
'''
a = np.array([1, 7, 11]) # 1D array
b = np.array([10, 20, 30])

vstack_array = np.vstack((a, b))
print(vstack_array)

a = np.array([[1, 2, 3], 
              [4, 5, 6]])

b = np.array([[-1,   -2,  -3], 
              [-4,   -5,  -6],
              [-7,   -8,  -9],
              [-10, -11, -12]])

print(np.vstack((a, b)))
# Stacking - Horizontal
'''
np.hstack((a1, a2, ...))
Stacks the arrays a1, a2, … horizontally (column-wise) in sequence and returns the stacked array.
Except in the case of 1D arrays, its equivalent to np.concatenate along axis = 1
'''
a = np.array([1, 7, 11]) 
b = np.array([10, 20])

print(np.hstack((a, b)))

a = np.array([[1, 2], 
             [3, 4],
             [5, 6]])

b = np.array([[-1,  -2,  -3,  -4], 
              [-5,  -6,  -7,  -8],
              [-9, -10, -11, -12]])

print(np.hstack((a, b)))

#Stacking
'''
np.stack(arrays, axis=0)
Joins a sequence of arrays along the new axis.
All input arrays must have the same shape.
The resulting array has 1 additional dimension compared to the input arrays.
The axis parameter specifies the index of the new axis which has to be created.
'''
a = np.array([1, 2, 3])
b = np.array([-1, -2, -3])

stacked_axis_0 = np.stack([a, b], axis = 0)
shape_axis_0 = stacked_axis_0.shape

print("stacked array with axis-0:\n", stacked_axis_0, "\n")

stacked_axis_1 = np.stack([a, b], axis = 1)
shape_axis_1 = stacked_axis_1.shape

print("stacked array with axis-1:\n", stacked_axis_1)




