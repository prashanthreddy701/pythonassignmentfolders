# np.repeat()
'''
numpy.repeat(arr, num_repeats, axis=None)
Repeats elements of an array.
Outputs an array which has the same shape as arr, except along the given axis.
num_repeats are the number of repetitions for each element which is broadcasted to fit the shape of the given axis.
'''
import numpy as np

print(np.repeat(2, 9)) # repeating 2 for five times 
a = np.array([[1,2,3],
              [4,5,6]])
print(np.repeat(a, 2, axis=0)) #repeating [1,2,3] for two times in vertical direction and then [4,5,6] for two times in horizontal direction 

a = np.array([[1,2,3],
              [4,5,6]])

print(np.repeat(a, 3, axis=1)) # repeating in horizontal direction for 3 times 

a = np.array([[1,2,4],
              [4,5,6]])

print(np.repeat(a, [3,5], axis=0)) # 3is for first row and columns is for second row 

a = np.array([[1,2,3],
              [4,5,6]])

print(np.repeat(a, [1,3,5], axis=1)) #1 is for 1st column and 3 is for 2nd column 5 is for 3 rd column
# np.delete()
'''
np.delete(arr, delete_indices, axis=None)
Returns a new array by deleting all the sub-arrays along the mentioned axis.
delete_indices indicates the indices of the sub-arrays that need to be removed along the specified axis.
'''
a = np.array([[ 1,  3,  5,  7],
              [ 2,  4,  6,  8],
              [-1, -2, -3, -4]])

delete_in_axis_0 = np.delete(a, 1, 0)
print("delete at position-1 along axis-0:\n", delete_in_axis_0)

a = np.array([[ 1,  3,  5,  7],
              [ 2,  4,  6,  8],
              [-1, -2, -3, -4]])

delete_in_axis_1 = np.delete(a, 2, 1)
print("delete at position-2 along axis-1:\n", delete_in_axis_1)

a = np.array([[ 1,  3,  5,  7],
              [ 2,  4,  6,  8],
              [-1, -2, -3, -4]])

print(np.delete(a, [0,3], axis=None)) # if we use None array will become flattend and we should delete based on the given axis


