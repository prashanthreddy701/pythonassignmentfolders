'''
Decorator 
it is a function
it always take function as argument(inputfunction)
it will create a new function with extended functionality
in this new function we may use original function
it will return the extended function as output 
'''
def decor(func):
    def inner():
        print("send a person to parlour")
        print("showing a person with full decoration")
    return inner
@decor
def display():
    print("showing a person as it iss")
display()
# here if there is @decor annotation then decorator function will take function as input argument and it will execute the returned function without executing the called function 
# without effecting original function we can extend its functionality using decorator
def decor_for_add(func):
    def inner(a,b):
        print("#" * 40)
        print("The sum of {} and {}:".format(a,b),end="")
        func(a,b)
        print("#" * 40)
    return inner
                           #inner function is replaced as add (should contain same number of arguments)
        
        
                                      #inner function output of decorator means innere will get executed 
        
@ decor_for_add
def add(a,b):
    print(a+b)

add(10,20)
print("------------------")

# 3rd example

def decor_for_sunny(func):
    def inner(name):
        names=["cm","pm"]
        if name in names:
            print("#"*50)
            print("hello {} you are very important".format(name))
        else:
            func(name)
    return inner
@decor_for_sunny
def wish(name):
    print("good morning",name)
wish("cm")
wish("ravi")
    
# Decorator chaining


