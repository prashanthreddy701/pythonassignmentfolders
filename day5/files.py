'''f=open('abc.txt','w')
f.write("ram\n")
f.write("rahim\n")
f.write("Robert\n")
print("data written to the file successfully")
f.close()


with open("abc.txt","w") as f:
    f.write("Prashanth reddy \n")
    f.write("mulug \n")
    f.write("warangal \n")
    print(f.closed)
print(f.closed)'''

# tell and seek methods
# we can use tell method to know the cursor position

'''f=open("abc.txt","r")
print(f.tell())
print(f.read(4))
print(f.tell()) # after reading 4 lines the cursor will come on 4th line
print(f.read()) # here we are reading complete data
print(f.tell()) # here cursor came to last letter

#seek method means move the cursor to the required position from begining position 
# we can use seek method to the required position

f=open("abc.txt","r")
f.seek(15)
print(f.tell())
'''
with open("abc.txt","r+") as f:
    text=f.read()
    print("data before modification")
    print("#" *50)
    print(text)
    f.seek(5)
    f.write("Gems")
    f.seek(0)
with open("abc.txt","w") as f:
    f.write("hello world")
print(f.closed)

file1 = open("abc.txt", "a")  
file1.write("\n You rock my world )")  
file1.close()

