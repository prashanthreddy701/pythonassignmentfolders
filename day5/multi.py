# By default every program consists of one main thread
from threading import *

def display():
    for i in range(10):
        print("hai Thread is executed by:\n")
        print(current_thread().getName())
#getName() − The getName() method returns the name of a thread.      
t=Thread(target=display) # creating a thread to execute display function 
t.start()       # starting of a thread 
for i in range(23):
        print("bye Thread is executed by:\n")
        print(current_thread().getName())
#Creating a Thread without using any class:
from threading import *
def display():
    for i in range(1,11):
        print("Child Thread")
t=Thread(target=display) #creating Thread object
t.start() #starting of Thread
for i in range(1,11):
    print("Main Thread")
# Creating a Thread by extending Thread class
#run() − The run() method is the entry point for a thread.
'''
We have to create child class for Thread class. In that child class we have to override run() method
Whenever we call start() method then automatically run() method will be
executed
'''
class MyThread(Thread):
    def run(self):
        for i in range(10):
            print("child Thread") 
t=MyThread()
t.start() # main thread starts child thread
for i in range(10):
    print('main thread')
#Creating a Thread without extending Thread class
class Test:
    def display(self):
        for i in range(10):
            print("child thread-2")
obj=Test()
t=Thread(target=obj.display)
t.start()
for i in range(10):
    print("main thread2")
    

            
        
    
