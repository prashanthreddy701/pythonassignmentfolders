#flatten() method
'''
We can use flatten() method to flatten(convert) any n-Dimensional array to 1-
Dimensional array.
'''
import numpy as np 
a = np.arange(6).reshape(3,2)
b = a.flatten()
print(f"Original array :\n {a}")
print(f"Flatten array :\n {b}")
a = np.array([10,20,30,40])
atrans = np.transpose(a)
print(f"Original Array : \n {a}")

array_3d = np.ones((2, 3, 4, 5, 6))
'''
np.moveaxis
np.moveaxis(arr, original_positions, new_positions)

Returns the view of an array with the axes moved from their original positions to the new positions.
Other axes remain in their original order.
'''
axis_moved_array = np.moveaxis(array_3d, [0, 1], [1, 2])
print(axis_moved_array.shape)

array = np.array([[1,   1,  1],
                  [-1, -1, -1],
                  [2,   2,  2],
                  [-2, -2, -2],
                  [3,   3,  3],
                  [-3, -3, -3]])

split_arrays = np.vsplit(array, [3])

print(split_arrays)
