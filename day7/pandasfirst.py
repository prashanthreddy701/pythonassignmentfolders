import pandas as pd
data_df=pd.read_csv("Data.csv")
print(data_df.shape)
# here shape is (10,4) so there are 10 rows and 4 columns including heading 
print(data_df.columns) # here gives the column names 
print(data_df.dtypes) # here dtypes gives the data types of each columns 
'''
df.head
df.head(n=5)
Returns the first n rows.
For negative values of n, this function returns all the rows except for the last n rows.
'''
print(data_df.head(n=6)) # returns n number of rows 
print(data_df.head(-3)) # except last 3 everything will be printed 
print(data_df.tail(3)) #returns last 3 rows 
print(data_df.tail(-3)) # except first 3 rows everything will be returned 
'''
df.describe
df.describe(include=None, exclude=None)
Generates descriptive statistics.
Analyzes both numeric and object series as well as mixed data types.
include: A list of data types to include in the result.
exclude: A list of data types to omit from the result.
'''
print(data_df.describe())
'''
Variance:
Variance is a simple measure of dispersion. Variance measures how far each number in the dataset from the mean
let say 4 values like 2,3,3,4
mean= sum of 4 values/ number of values 
mean=3 
variance =(single number-mean)2+...../number of values 
Standard Deviation:
Standard deviation is a squared root of the variance to get original values. Low standard deviation indicates data points close to mean.
'''

