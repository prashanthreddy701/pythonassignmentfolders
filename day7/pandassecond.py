import pandas as pd 
data_df=pd.read_csv("Data.csv")
print(data_df.info()) 
print(data_df['Country'][6]) # it gives 6th index country name 
print(data_df.iloc[0,2]) # it selects  based on indexing like in first row only second column is selected
print("-----")
print(data_df.iloc[[0,4],[0,3]]) 
print(data_df.iloc[[0,9],[1,3]])



'''
 here in first[0,4] it selects 0 th row and 4 th row then after that in that 0throw and 4th row oth column and 3rd column are selected 
 '''
print(data_df)
print(data_df.loc[0]) # it gives complete information about first row 
print(data_df.loc[[0,1]]) # it gives complete information about 0th row and 1st row
print(data_df.loc[[0,1],['Country','Purchased']]) # it gives Country and purchased Columns in 0th row and 1 st row 
print(data_df.loc[0,"Country"])
print(data_df[0:3]) #here 3 index is not included 
print(data_df.loc[0:3]) # here 3 index is included in loc method both start and stop are included 
print(data_df.loc[0:3,"Country"]) # here 0th row to 3rd row with column name country 
pd.set_option('display.max_rows', 8) # here max 8 rows will be displayed remaining will be displayed by ...........................
print(data_df)
pd.reset_option('display') # to reset all the display options 
print(data_df.set_index(["Country","Age"])) # it returns a new data frame with country and age as new indexes 
# identifiers for rows is called index 
#here in data frame if we change anything original data frame will be remain as it is 
# if we want to change we have to add shopping_df.set_index('Order ID', inplace = True) To change permanently have to use this 
print("--------")
# by default it sorts in ascending order so inorder to sort in descending order use ascending =False 
print(data_df.sort_index(ascending=False)) 
