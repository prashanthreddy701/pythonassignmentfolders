import logging
logging.basicConfig(filename="mylog.txt",level=logging.INFO,format="%(asctime)s:-%(levelname)s:%(message)s",datefmt="%d/%m/%Y % I:%M:%s %p")
logging.info(" request came")
try:
    a=int(input("Please Enter First Number"))
    b=int(input("Please Enter Second Number"))
    print("Result",a/b)
except ZeroDivisionError as msg:
    print("cannot divide with zeros")
    logging.exception(msg)
except ValueError as msg:
    print("provide int values only")
    logging.exception(msg)
logging.info("Request Processing Completed")
