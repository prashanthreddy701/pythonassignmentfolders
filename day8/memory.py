# This is a python module for monitoring memory consumption of a process as well as line-by-line analysis of memory consumption for python programs.
from memory_profiler import profile
from time import time, ctime 

@profile
def compute_factorial(n):
    start_time=time()
    print("start_time",ctime(start_time))
    if n<=1:
        return 1 
    else:
        end_time=time()
        print("end_time",ctime(end_time))
        return n * compute_factorial(n-1)
num = int(input())
# Call the compute_factorial function
factorial=compute_factorial(num)
