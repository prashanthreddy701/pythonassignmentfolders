import pandas as pd 
data_df=pd.read_csv("Data.csv") 
# data_df.drop(columns=["Age"],inplace=True) it completely deletes the column if we write inplace =True 
print(data_df)
def change_country_name(Country):
  new_name = Country.lower()
  return new_name 
x=data_df["Country"].apply(change_country_name) 
print(x) 
people = {
    "full_name": ["Jack Smith", 'Jane Lodge', 'John Doe', 'Kristen Carol'], 
    "email": ["JackSmith@gmail.com", 'JaneLodge@email.com', 'JohnDoe@email.com', 'KristenC@email.com']
}
people_df = pd.DataFrame(people)
y=people_df.applymap(len)
print(y)
z=data_df['Country'].map({"Spain":"Ukraine"})
print(z)
print("-------------->")
data_df['Country'].replace({"Spain":"Us","France":"Russia","Germany":"Canada"})
print(data_df)  
#replace accepts str, regex, list, dict, Series, int, float, or None. map accepts a dict or a Series 
