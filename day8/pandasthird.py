import pandas as pd
data_df=pd.read_csv("Data.csv")
age_filter = data_df['Age'] > 30
#The boolean expression returns a series containing True and False boolean values.
print(age_filter)
# here we will get new dataset by satisfying the given condition here all rows with age > 30 will be displayed 
print(data_df[age_filter])
# here age of the employees is displayed (more than 30)
print(data_df.loc[age_filter, 'Age'])
# here age along with salary is displayed
print(data_df.loc[age_filter, 'Age':'Salary'])
age_range_filter = (data_df['Age'] > 30) & (data_df['Age'] < 40)
print(data_df.loc[age_range_filter])
# here in negative filter >=60000 so employees whose salary is >60000 are equal to will not be displayed 
print("negative filter:")
neg_filter = (data_df['Salary'] >= 60000)
print(data_df[~neg_filter])
print("salary:")
print(data_df['Salary'])
print("string containing")
filter = data_df['Country'].str.contains('France')
print(data_df[filter])
print("string ends with")
filter = data_df['Country'].str.endswith('ain')
print(data_df[filter])
