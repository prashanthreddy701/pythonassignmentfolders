import re
import pandas as pd
import seaborn as sns
import nltk
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix,accuracy_score
from sklearn.naive_bayes import MultinomialNB
fake_df=pd.read_csv('train.csv.zip')
fake_df.head()
# checking empty values
fake_df.isna().sum()
fake_df.dropna(axis=0,inplace=True)
print(fake_df.shape)
fake_df.reset_index(inplace=True)
# balanced or imbalanced
sns.set_theme(style="darkgrid")
sns.countplot(fake_df["label"])
second=fake_df["title"][0]
lower=re.sub("[^a-zA-z]"," ",second)
result=lower.lower()
splitting_into_list=result.split()
nltk.download("stopwords")
ps=PorterStemmer()
review=[]
for word in splitting_into_list:
    if word not in set(stopwords.words("english")):
        review.append(ps.stem(word))
        print(review)
" ".join(review)
corpus=[]
for i in range(0,len(fake_df)):
    x=fake_df["title"][i]
    eliminating_other_than_alphabets=re.sub("[^a-zA-z]"," ",x)
    eliminating_other_than_alphabets=eliminating_other_than_alphabets.lower()
    list1=eliminating_other_than_alphabets.split()
    stemming_the_words=[ps.stem(word)
    for word in list1 if not word in set(stopwords.words("english"))]
    corpus.append(" ".join(stemming_the_words))

# creating bag of words model
cv=CountVectorizer()
X=cv.fit_transform(corpus).toarray()
y=fake_df["label"]
x_train, x_test, y_train, y_test=train_test_split(X,y,test_size=0.25,random_state=0)
classifier=MultinomialNB()
classifier.fit(x_train,y_train)
y_pred=classifier.predict(x_test)
cm=confusion_matrix(y_test,y_pred)
sns.heatmap(cm,annot=True)
accuracy_score(y_test,y_pred)


X_train_prediction = classifier.predict(x_train)
training_data_accuracy = accuracy_score(X_train_prediction, y_train)
print(training_data_accuracy)

X_test_prediction = classifier.predict(x_test)
test_data_accuracy = accuracy_score(X_test_prediction, y_test)
accuracy_score(X_test_prediction,y_test)

# downloading test data
test_data = pd.read_csv("test.csv")

test_data.shape

test_data.isnull().sum()

test_data = test_data.fillna('')
test_data.isnull().sum()

test_data['content'] = test_data['author']+" "+test_data['title']
print(test_data['content'])

def myfunction(content):

    eliminating_other_than_alphabets=re.sub("[^a-zA-z]"," ",str(content))
    eliminating_other_than_alphabets=eliminating_other_than_alphabets.lower()
    list1=eliminating_other_than_alphabets.split()
    stemming_the_words=[ps.stem(word)
    for word in list1 if not word in set(stopwords.words("english"))]
    stemmed_content=" ".join(stemming_the_words)
    return stemmed_content
test_data['content'] = test_data['content'].apply(myfunction)
print(test_data["content"])

test_X = test_data['content'].values

# applying the model
cv=CountVectorizer()
test_X=cv.fit_transform(test_X)

test_X.shape

predicting_the_test_x = classifier.predict(test_X)

# downloading the submission.csv file
output = pd.DataFrame({'id': test_data.id,
                      'label': predicting_the_test_x})

output.to_csv('submission.csv', index=False)
print(output)
