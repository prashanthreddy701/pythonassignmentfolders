import numpy as np
x = np.array([1, 2, 3,7])
print(x.ndim)
array_2d=np.array([[1,2,3],[4,5,6],[7,8,9]])
print(array_2d.ndim)
a=np.array([[[1,2,3],
            [4,5,6]],
           
            [[7,8,9],
            [10,11,12]]])
print(a.ndim)
# here dimensions means axes 1 dimension means one axis and two dimensions means two axes three dimension means three axis
#shape shows number of rows and columns
print(x.shape)
print(array_2d.shape)
print(a.shape) #numpy reports the shape of 3D arrays in the order layers, rows, columns.
#Reshape
#It changes the shape of the ndarray without changing the data within the ndarray:
print(np.reshape(x,(2,2))) # here single row is converted to two rows and two columns by using reshaping 
a = np.array([3,6,9,12])
print(np.reshape(a,(2,2)))
#how many values there are in the array using the size attribute
print(x.size)
print(array_2d.size)
print(a.size)

