import numpy as np
array_1=np.arange(5)
array_2=array_1.reshape(5,1)
# fancy indexing
array_3=array_2[[2,3]] # here I have selected two rows by using fancy indexing 
print(array_3)
array_3=array_1.reshape(5,1)
array_4=array_1[[0,2]] # here I have selected two coumns by using fancy indexing 
print(array_4)
'''
array_name[row,column]
array_name[begin:end:step,begin:end:step]
'''
a = np.array([[10,20],[30,40],[50,60]])
result_array_1=a[:,:]
print(result_array_1) # here all rows and columns are selected
result_array_2=a[::2,::2]
print(result_array_2)
a=np.arange(1,17).reshape(4,4)
a_1=np.arange(1,17).reshape(16,1)
a_2=np.arange(1,17).reshape(1,16)
print(a,a.ndim)
print(a_1,a_1.ndim)
print(a_2,a_2.ndim)
a = np.arange(1,25).reshape(2,3,4) # 2 layers in 2 layers there are 3 rows and 4 columns
# array_name[begin:end:step,begin:end:step,begin:end:step]
# all indices of 2d array all rows in 2 layers all columns in 2 layers 
result_1=a[:,:,:]
print(result_1)
result_2=a[:,1:2,:] 
print(result_2)
result_3=a[0:1,::2,1::2]
print(result_3) #first layer is selected in that all rows with step size 2 so that
#(0th and 2nd row are selected) and in the column only one column is selected

