# importing libraries
import nltk
from nltk.stem.porter import PorterStemmer
from nltk.stem.snowball import SnowballStemmer
import spacy
import spacy.cli
from nltk.corpus import stopwords
# small english pipe line
spacy.cli.download("en_core_web_sm")
nlp=spacy.load("en_core_web_sm")

doc = nlp("Backgammon is one of the oldest known board games.")
for token in doc:
    print(token.text,token.pos_,token.dep_,token.lemma_,token.tag)
  # parts of speech , describes relationship with parents
  # lemma converts past in present v2 form in v1 form based on condition
for chunk in doc.noun_chunks:
    print(chunk.text)
p_stem=PorterStemmer()
words=["run","runner","running","ran","runs","easily","fairly"]
for i in words:
    print(i +"......."+p_stem.stem(i))

s_stemmer=SnowballStemmer(language="english")
words=["run","runner","running","ran","runs","easily","fairly"]
for i in words:
    print(i +"........"+s_stemmer.stem(i))

 # stop words are the most repeated words which are not used mostly
nltk.download("stopwords")
stop_words=stopwords.words("english")
len(stop_words)

