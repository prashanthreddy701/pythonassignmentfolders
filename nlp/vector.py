# import libraries
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import spacy
a=["This is india india is my country"]
b=["Iam living in india"]

# CountVectorizer object is created
vectorizer=CountVectorizer()
vectorizer.fit(a)

print(vectorizer.vocabulary_)
newvector=vectorizer.transform(b)
# new vector will displayed with if one word matches with other
print(newvector.toarray())

text=["I love India","India is my country","India got independence on august 15"]
vectorizer=TfidfVectorizer()
vectorizer.fit(text)
print(vectorizer.idf_)
print(vectorizer.vocabulary_)
# Named Entity Recognition
# GPE geological Political Entity
# FAC places, Temples
NER = spacy.load("en_core_web_sm")
text1= NER("Narendra modi is the prime minister of india. New Delhi is the capital city of india.")
for word in text1.ents:
    print(word.text+"........."+word.label_)
