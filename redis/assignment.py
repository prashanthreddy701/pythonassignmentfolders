from flask import Flask
import redis


# make redis
redis_cache = redis.Redis(host='localhost', port=6379, db=0)

# make flask app
app = Flask(__name__)

# setting the data in redis
@app.route('/set/<string:key>/<string:value>')
def set(key, value):
    if redis_cache.exists(key):
        pass
    else:
        redis_cache.set(key, value)
    return "OK"

# getting the data in redis
@app.route('/get/<string:key>')
def get(key):
    if redis_cache.exists(key):
        return redis_cache.get(key)
    else:
        return f"{key} is not exists"
# setting the data in redis using list
@app.route('/add_to_list/<string:key>/<string:value>')
def add_to_list(key,value):
    redis_cache.lpush(key, value)
    return "OK"
# Retrieving the data
@app.route('/get_list/<string:key>')
def get_list(key):
    if redis_cache.exists(key):
        result = redis_cache.lrange(key,start=0,end=-1)
        print(result)
        return "OK"

    else:
        return f"{key} is not exists"


if __name__=="__main__":
    app.run()

