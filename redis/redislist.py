# importing redis
import redis

r=redis.Redis(host="localhost",port=6379,db=0)
# collegenames is the name of the list
collegenames="COLLEGE"
# rpush means pushes from right side
# lpush means pushes from left side
r.rpush(collegenames,"GRIET")
r.rpush(collegenames,"CBIT")
r.rpush(collegenames,"VNR")
r.rpush(collegenames,"CVR")
r.rpush(collegenames,"KITS")
# llen returns length of the list
# rpop Removes and gets the last element in a list
while r.llen(collegenames)!=0:
    print(r.rpop(collegenames))
