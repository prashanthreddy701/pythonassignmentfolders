import csv
import cv2
import pytesseract
import redis


r = redis.Redis(host='localhost', port=6379, db=1)
def ocr_output(IMG):
    TEXT = pytesseract.image_to_string(IMG)
    return TEXT
img = cv2.imread("/home/neosoft/Documents/myassignmentspython/redispractice/recipeimages/biscuits.png",cv2.IMREAD_UNCHANGED)
print('Original Dimensions : ', img.shape)

croptitle = img[0:30, 150:500]
cv2.imshow('CropImage1', croptitle)
cropingred = img[30:90, 150:500]
cv2.imshow('CropImage2', cropingred)
cropsteps = img[80:300, 150:500]
cv2.imshow('CropImage3', cropsteps)
RES = ocr_output(croptitle)
fin_res = "Title of the Recipe : " + RES
r.set('Recipe Title', RES) # True
value = r.get('Recipe Title')
print(value)
RES = ocr_output(cropingred)
fin_res1 = "Ingrediemts of the Recipe : " + RES
r.set('Recipe Ingredients', RES) # True
value1 = r.get('Recipe Ingredients')
print(value1)
RES = ocr_output(cropsteps)
fin_res2 = "steps of the Recipe : " + RES
r.set('Cooking steps', RES) # True
value2 = r.get('Cooking steps')
print(value2)
cv2.waitKey(0)
cv2.destroyAllWindows()



