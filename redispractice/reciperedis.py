from PIL import Image
import pytesseract as pt
import os
import redis


r = redis.Redis(host='localhost', port=6379, db=1)


def main():
    # path for the folder for getting the raw images
    path = "/home/neosoft/Documents/myassignmentspython/redispractice/recipeimages"
    for imageName in os.listdir(path):
        inputPath = os.path.join(path, imageName)
        img = Image.open(inputPath)
        text = pt.image_to_string(img, lang="eng")
        r.set('Title', text)
        value = r.get('Title')
        print(value)


if __name__ == '__main__':
    main()