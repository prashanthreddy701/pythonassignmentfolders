import base64
import cv2
import os
import time
import zmq
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.bind("tcp://*:2000")
# reading file
f = open("room_ser.jpg",'rb')
bytes = bytearray(f.read())
socket.send(bytes)
